let button = document.getElementById("button");
let input = document.getElementById("input");
let answer = document.getElementById("answer");
let eight = document.getElementById("eight");
let options = [
    "This is correct",
    "The fates smile down on you",
    "Without a doubt",
    "Definitely",
    "Count on it",
    "It is true",
    "The odds are in your favor",
    "Yes",
    "Agreed",
    "Unsure",
    "Ask again later",
    "Undecided",
    "Still thinking of an answer",
    "Rephrase and ask again",
    "No",
    "The outlook isn't good",
    "Don't count on it",
    "Unlikely",
    "The fates won't allow it"
]
  button.addEventListener("click", function(){
    if (input.value.length < 1) {
      alert("Please enter a question!");
    } else {
      eight.innerText = "";
      let num = input.value.length % options.length;
      answer.innerText = options[num];
    }
  });

